const path = require('path');

module.exports = {
    entry: {
        app: './src/app.js' //src file
    },
    output: {
        path: path.resolve(__dirname, 'build'), //compiled js
        filename: 'app.bundle.js'
    },
    module: { //babel loader
        rules: [{
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['env']
            },
            
        }]
    }
}
