{
  function getVariables(t, ts, tss) {
    if (t) {
      var arr = [];
      var arr2 = [];
      for (var i = 0; i < ts.length; ++i) {
        arr.push(ts[i][2]);
      }
      var object = Object.assign({}, ...[t].concat(arr))
      return object;
    }
  }
  
    function getFormulas(t, ts, tss) {
    if (t) {
      var arr = [];
      var arr2 = [];
      for (var i = 0; i < ts.length; ++i) {
        arr.push(ts[i][2]);
      }
            for (var i = 0; i < tss.length; ++i) {
        arr2.push(tss[i][2]);
      }
      return t.concat(arr).concat(tss[2]);
    }
  }
  

  function getQual(q) {
    return q == "A." || q.toLowerCase() == "forall" || q == "∀"? "forall" : "exists";
  } 
}

start = formulas

matchNeg  = "not"i / "!" / "~" / "¬"
matchQual = "A." / "forall"i / "∀"
matchImp  = "implies"i / "=>" / "⇒"
matchOr   = "or"i / "v" / "∨"
matchAnd  = "and"i / "∧"
matchExists = "exists"i / "E." / "∃"
matchForall = "A." / "forall"i / "∀"


formulas = p:imp* vs:("," _ imp)* _ vss:("⊢"_ imp)? {return {formulas:getFormulas(p,vs,vss)}}

imp  = x:or _ matchImp _ y:imp { return { implies: {left: x, right: y } }; }  / or

or = left:and _ right:(a:_ matchOr _ b:and 
{return {right:b}})* 
  {return (right.reduce((t,h) => ({or: {left:t,...h}}), left));
 } / and
  
  
and = left:neg _ right:(a:_ matchAnd _ b:neg {return {right:b}})* 
   {return right.reduce((t,h) => ({and: {left:t,...h}}), left);
  } / neg

neg  = matchNeg _ x:neg { return { not: x }; } / exists

exists = q:matchExists _ v:variable vs:("," _ variable)* _ fm:exists {
    return { exists: {variables: getVariables(v, vs), formula: fm } };} 
    / forall
    
forall = q:matchForall _ v:variable vs:("," _ variable)* _ fm:forall {
    return { forall: {variables: getVariables(v, vs), formula: fm } };} 
    / brackets
    
brackets
  = "(" _ expr:imp _ ")" { return expr; }
  / "[" _ expr:imp _ "]" { return expr; }
  / pred

pred = first:[a-zA-Z] rest:[a-zA-Z0-9]* ts:variables? {
    return { predicate: { name: (first + rest.join("")), args: (ts? ts : undefined) } };
  } 
  

variables = "(" _ t:variable? ts:("," _ variable)* _ ")" { return getVariables(t, ts); }
variable = first:[a-z] rest:[a-zA-Z0-9]* { return { kind: "variable", name: (first + rest.join("")) }; }

_  = [ \t\r\n]* 
__ = [ \t\r\n]+






