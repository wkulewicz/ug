//IMPORTY
import $ from 'jquery'
import popper from 'popper.js'
import pegjs from 'pegjs'
import parser from '../parser_sequent.js' // tworzy listę wprowadzonych formul, ostatnia jest wnioskiem
import parser2 from '../parser_formula.js'
import {treeNode,proof,formula} from './lib';

//STAŁE
const addProof = document.getElementById("addProof");
const proofsList = document.getElementById('proofsList');
const main = document.getElementById("main");
const contradiction = {"contradiction": "⊥"};
const exportJsonDataButton = document.getElementById("exportJsonDataButton");
const ProofObject = require('./lib');
const Sign = document.getElementsByClassName("sign");
const newProof = document.getElementById("new_proof")
var allProofs = [{
	"presentation": "E v F ⊢ F v E",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"or": {
						"left": {
							"predicate": {
								"name": "E"
							}
						},
						"right": {
							"predicate": {
								"name": "F"
							}
						}
					}
				},
				"uniqId": "ca6c50d4-9292-4f8c-bffa-87f4ee6b6308",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"or": {
					"left": {
						"predicate": {
							"name": "F"
						}
					},
					"right": {
						"predicate": {
							"name": "E"
						}
					}
				}
			},
			"uniqId": "cf57e61b-7e11-4d84-91ce-04b3db7f5b50",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                  {
	"presentation": "(P ∧ Q) ∧ R ⊢ Q ⇒ (R ⇒ P)",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"and": {
						"left": {
							"and": {
								"left": {
									"predicate": {
										"name": "P"
									}
								},
								"right": {
									"predicate": {
										"name": "Q"
									}
								}
							}
						},
						"right": {
							"predicate": {
								"name": "R"
							}
						}
					}
				},
				"uniqId": "15b0b3a3-fa27-424d-ae5d-55abfdce06ab",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"implies": {
					"left": {
						"predicate": {
							"name": "Q"
						}
					},
					"right": {
						"implies": {
							"left": {
								"predicate": {
									"name": "R"
								}
							},
							"right": {
								"predicate": {
									"name": "P"
								}
							}
						}
					}
				}
			},
			"uniqId": "44b4fe84-e977-44b8-8f24-e69e4021e31c",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                  {
	"presentation": " ⊢ ((E ⇒ F) ⇒ F) ⇒ ((F ⇒ E) ⇒ E)",
	"tree": {
		"assumptions": [],
		"conclusion": {
			"formulaTree": {
				"implies": {
					"left": {
						"implies": {
							"left": {
								"implies": {
									"left": {
										"predicate": {
											"name": "E"
										}
									},
									"right": {
										"predicate": {
											"name": "F"
										}
									}
								}
							},
							"right": {
								"predicate": {
									"name": "F"
								}
							}
						}
					},
					"right": {
						"implies": {
							"left": {
								"implies": {
									"left": {
										"predicate": {
											"name": "F"
										}
									},
									"right": {
										"predicate": {
											"name": "E"
										}
									}
								}
							},
							"right": {
								"predicate": {
									"name": "E"
								}
							}
						}
					}
				}
			},
			"uniqId": "7de8d09a-e9da-4dcf-9e7d-459c95b1d68f",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                   {
	"presentation": "E ⇒ (F ⇒ G) ⊢ (E ⇒ F) ⇒ (E ⇒ G)",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"implies": {
						"left": {
							"predicate": {
								"name": "E"
							}
						},
						"right": {
							"implies": {
								"left": {
									"predicate": {
										"name": "F"
									}
								},
								"right": {
									"predicate": {
										"name": "G"
									}
								}
							}
						}
					}
				},
				"uniqId": "c68d7745-5af3-4582-aed8-1dfd7e061003",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"implies": {
					"left": {
						"implies": {
							"left": {
								"predicate": {
									"name": "E"
								}
							},
							"right": {
								"predicate": {
									"name": "F"
								}
							}
						}
					},
					"right": {
						"implies": {
							"left": {
								"predicate": {
									"name": "E"
								}
							},
							"right": {
								"predicate": {
									"name": "G"
								}
							}
						}
					}
				}
			},
			"uniqId": "a9341be2-52e0-4533-9bfc-88f4eeff46e6",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                   {
	"presentation": "E ⇒ F, E ⊢ F",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"implies": {
						"left": {
							"predicate": {
								"name": "E"
							}
						},
						"right": {
							"predicate": {
								"name": "F"
							}
						}
					}
				},
				"uniqId": "b4df0368-6c14-4f02-83cb-00a4b19ce2c1",
				"reason": "assumption"
			},
			{
				"formulaTree": {
					"predicate": {
						"name": "E"
					}
				},
				"uniqId": "0b5c56c4-f288-4ac3-9314-1ba457549b83",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"predicate": {
					"name": "F"
				}
			},
			"uniqId": "f56765d0-5e98-4d3f-93c4-930784867e2c",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                   {
	"presentation": "¬E ⊢ E ⇒ F",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"not": {
						"predicate": {
							"name": "E"
						}
					}
				},
				"uniqId": "ae918d42-6517-43aa-b92c-f15ca8b69bc8",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"implies": {
					"left": {
						"predicate": {
							"name": "E"
						}
					},
					"right": {
						"predicate": {
							"name": "F"
						}
					}
				}
			},
			"uniqId": "5e9cb1b4-944d-4e87-b883-9c208e8e9eaf",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                   {
	"presentation": "E ⊢ ¬E ⇒ F",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"predicate": {
						"name": "E"
					}
				},
				"uniqId": "5193299d-acc1-429d-b8f9-122fefb47f35",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"implies": {
					"left": {
						"not": {
							"predicate": {
								"name": "E"
							}
						}
					},
					"right": {
						"predicate": {
							"name": "F"
						}
					}
				}
			},
			"uniqId": "43ca4520-225a-4a51-b3ad-cf06bc15d5bd",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
},
                 {
	"presentation": "P, P ⇒ Q ⊢ Q",
	"tree": {
		"assumptions": [
			{
				"formulaTree": {
					"predicate": {
						"name": "P"
					}
				},
				"uniqId": "b9d6fa3d-b0b8-43ff-93c2-4be5d8053972",
				"reason": "assumption"
			},
			{
				"formulaTree": {
					"implies": {
						"left": {
							"predicate": {
								"name": "P"
							}
						},
						"right": {
							"predicate": {
								"name": "Q"
							}
						}
					}
				},
				"uniqId": "1e8a02c7-81b4-401c-a835-1a87142334b6",
				"reason": "assumption"
			}
		],
		"conclusion": {
			"formulaTree": {
				"predicate": {
					"name": "Q"
				}
			},
			"uniqId": "fbb53187-99d3-4640-81ee-a5097e790308",
			"reason": null
		},
		"box": null,
		"left": null,
		"right": null,
		"isRoot": true
	},
	"isSolved": false
}
                ];




exportJsonDataButton.addEventListener('click', function () {
    var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(allProofs));
    exportJsonDataButton.href = 'data:' + data;
    exportJsonDataButton.download = 'logUG-'+  new Date().toLocaleDateString()   +'.json';
});
document.getElementById('import').onclick = function () {
        var files = document.getElementById('selectFiles').files;
        console.log(files);
        if (files.length <= 0) {
            return false;
        }

        var fr = new FileReader();

        fr.onload = function (e) {
            console.log(e);
            var result = JSON.parse(e.target.result);
            allProofs = result;
            refreshProofs();
        }

        fr.readAsText(files.item(0));
    };




function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
Array.from(Sign).forEach(function (element, Index) {
    element.addEventListener('click', function () {
        newProof.value += element.textContent;
        newProof.focus();

        try {
            document.getElementById("result").textContent = "";
            var result = parser.parse(document.getElementById("new_proof").value);
            addProof.disabled = false;
        } catch (err) {
            createTool(tree);
        }
    });

}); //dodawanie znaków specjalnych do okna dialogowego
newProof.addEventListener('input', function () {
    try {
        document.getElementById("result").textContent = "";
        var result = parser.parse(document.getElementById("new_proof").value);
        addProof.disabled = false;
    } catch (err) {
        document.getElementById("result").textContent = err.toString();
        addProof.disabled = true;
    }

}); //pogląd walidacji wprowadzanego sekwentu w modalu
addProof.addEventListener('click', function () {
    try {
        document.getElementById("result").textContent = "";
        let result = parser.parse(newProof.value);
        let treeNodeObject = new treeNode(
            getAssumptions(result.formulas),
            getConclusion(result.formulas),
            null,
            null,
            null,
            true
        );

        let proofObject = new proof(
            getPresentation(treeNodeObject),
            treeNodeObject,
            false
        )
        

        console.log(JSONstringify(proofObject));

        allProofs.push(proofObject);
        newProof.value = "";
        refreshProofs();

    } catch (err) {
        document.getElementById("result").textContent = err.toString();
    }
});

function refreshProofs() {
    proofsList.innerHTML = '<a href="#" class="list-group-item active" data-toggle="modal" data-target="#myModal">Nowy dowód</a>';
    allProofs.forEach(function (proof) {
        let a = document.createElement('a');
        a.className = "list-group-item";
        proofsList.appendChild(a);
        a.setAttribute('href', '#');
        a.innerHTML += proof.presentation;
  
        if(proof.isSolved){
            a.innerHTML += '<span style="color:green" class="glyphicon glyphicon-ok pull-right"></span>';
        } else{
             a.innerHTML += '<span style="color:red" class="glyphicon glyphicon-remove pull-right"></span>';
        }

        a.addEventListener('click', function () {
            createTool(proof.tree);

        });
    })
    return allProofs;
}; //odsiwiezenie listy sekwentow po lewej stornie
function createTool(root) {
    
    main.innerHTML = "";
    JSONstringify(root);
    renderNode(root,root);
    
    getReason();
    isSolved(root);
    
    
    
    return true;
}


//TREE FUNCTIONS
function renderNode(root,treeNode) {
    console.log('renderNodeStarts...');
    //jezeli wezel jest korzeniem 
    if (treeNode.isRoot) {
        for (let i = 0; i < treeNode.assumptions.length; i++) {
            main.appendChild(createFormulaButton(
                root, 
                treeNode.assumptions[i].formulaTree,
                removebrackets(traverse(treeNode.assumptions[i].formulaTree), 0),
                treeNode.assumptions[i].uniqId,
                treeNode,
                treeNode.assumptions[i].reason

            ));
        };

        if (treeNode.conclusion.reason == null) {
            main.appendChild(createFormulaButton(
                root,
                null,
                '...',
                null,
                treeNode,
                null
            ));

            main.appendChild(createFormulaButton(
                root,
                    treeNode.conclusion.formulaTree,
                    removebrackets(traverse(treeNode.conclusion.formulaTree)),
                    treeNode.conclusion.uniqId,
                    treeNode),
                    treeNode.conclusion.reason
            );
        } else {
            main.appendChild(createFormulaButton(
                root,
                treeNode.conclusion.formulaTree,
                removebrackets(traverse(treeNode.conclusion.formulaTree)),
                treeNode.conclusion.uniqId,
                treeNode,
                treeNode.conclusion.reason
            ));
        };
    };

    //przejscie lewego poddrzewa
    if (treeNode.left) {

        
        //jezeli rozni sie ilosc przeslanek oraz wniosek w stosunku do poprzednika
        if ((treeNode.assumptions.length < treeNode.left.assumptions.length) &&
            (treeNode.conclusion.uniqId != treeNode.left.conclusion.uniqId)) {
            console.log('NEW BOX');

            
            let div = document.createElement('div');
            div.className = "box";
            
            
            //utworzenie przycisku nowej przeslanki
            let newPremise = createFormulaButton(
                root,
                treeNode.left.assumptions.last().formulaTree,
                removebrackets(traverse(treeNode.left.assumptions.last().formulaTree)),
                treeNode.left.assumptions.last().uniqId,
                treeNode.left,
                treeNode.left.assumptions.last().reason
            );

            //dodajemy za ostatnia przeslanka ojca

            //jezeli wniosek lewego drzewa nie jest wyjasniony to
            if (treeNode.left.conclusion.reason == null) {
                let space = createFormulaButton(
                    root,
                    null,
                    '...',
                    null,
                    treeNode.left
                );

                let newConclusion = createFormulaButton(
                    root,
                    treeNode.left.conclusion.formulaTree,
                    removebrackets(traverse(treeNode.left.conclusion.formulaTree)),
                    treeNode.left.conclusion.uniqId,
                    treeNode.left,
                    treeNode.left.conclusion.reason
                );

                div.appendChild(newPremise);
                div.appendChild(space);
                div.appendChild(newConclusion);


                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode.insertBefore(
                        div,
                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode)
        

                renderNode(root,treeNode.left);

            } else if (treeNode.left.conclusion.reason) {
                

                let newConclusion = createFormulaButton(
                    root,
                    treeNode.left.conclusion.formulaTree,
                    removebrackets(traverse(treeNode.left.conclusion.formulaTree)),
                    treeNode.left.conclusion.uniqId,
                    treeNode.left,
                    treeNode.conclusion.reason
                );

                div.appendChild(newPremise);
                div.appendChild(newConclusion);

                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode.insertBefore(
                        div,
                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode)
        


                renderNode(root,treeNode.left);
            };

        } else if (treeNode.assumptions.length < treeNode.left.assumptions.length) {
            console.log('ADD PREMISE');
            let newButton = createFormulaButton(
                root,
                treeNode.left.assumptions.last().formulaTree, 
                removebrackets(traverse(treeNode.left.assumptions.last().formulaTree)), 
                treeNode.left.assumptions.last().uniqId,
                treeNode.left,
                treeNode.left.assumptions.last().reason 
            );

            insertAfter(
                newButton,
                document.getElementById(treeNode.assumptions.last().uniqId).parentNode.parentNode
            );

            renderNode(root,treeNode.left);
        } else if (treeNode.conclusion.uniqId != treeNode.left.conclusion.uniqId) {
            //insert before uniq id
            console.log('ADD CONCLUSION');
                let space = createFormulaButton(
                    root,
                    null,
                    '...',
                    null,
                    treeNode.left
                );
            
            let newConclusion = createFormulaButton(
                root,
                treeNode.left.conclusion.formulaTree,
                removebrackets(traverse(treeNode.left.conclusion.formulaTree)),
                treeNode.left.conclusion.uniqId,
                treeNode.left,
                treeNode.left.conclusion.reason
            );
            
            if(treeNode.left.conclusion.reason==null){
            document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode
                .insertBefore(
                space,
                document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode
            )
                }
            
            document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode
                .insertBefore(
                newConclusion,
                document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode
            )
            
            
            renderNode(root,treeNode.left);
            
            
        };
    };
 if (treeNode.right) {

        
        //jezeli rozni sie ilosc przeslanek oraz wniosek w stosunku do poprzednika
        if ((treeNode.assumptions.length < treeNode.right.assumptions.length) &&
            (treeNode.conclusion.uniqId != treeNode.right.conclusion.uniqId)) {
            console.log('NEW BOX');

            
            let div = document.createElement('div');
            div.className = "box";
            
            
            //utworzenie przycisku nowej przeslanki
            let newPremise = createFormulaButton(
                root,
                treeNode.right.assumptions.last().formulaTree,
                removebrackets(traverse(treeNode.right.assumptions.last().formulaTree)),
                treeNode.right.assumptions.last().uniqId,
                treeNode.right,
                treeNode.right.assumptions.last().reason
            );

            //dodajemy za ostatnia przeslanka ojca

            //jezeli wniosek lewego drzewa nie jest wyjasniony to
            if (treeNode.right.conclusion.reason == null) {
                let space = createFormulaButton(root,
                    null,
                    '...',
                    null,
                    treeNode.right
                );

                let newConclusion = createFormulaButton(
                    root,
                    treeNode.right.conclusion.formulaTree,
                    removebrackets(traverse(treeNode.right.conclusion.formulaTree)),
                    treeNode.right.conclusion.uniqId,
                    treeNode.right,
                    treeNode.right.conclusion.reason
                );

                div.appendChild(newPremise);
                div.appendChild(space);
                div.appendChild(newConclusion);


                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode.insertBefore(
                        div,
                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode)
        

                renderNode(root,treeNode.right);

            } else if (treeNode.right.conclusion.reason) {
                

                let newConclusion = createFormulaButton(
                    root,
                    treeNode.right.conclusion.formulaTree,
                    removebrackets(traverse(treeNode.right.conclusion.formulaTree)),
                    treeNode.right.conclusion.uniqId,
                    treeNode.right,
                    treeNode.conclusion.reason
                );

                div.appendChild(newPremise);
                div.appendChild(newConclusion);

                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode.insertBefore(
                        div,
                    document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode)
        


                renderNode(root,treeNode.right);
            };

        } else if (treeNode.assumptions.length < treeNode.right.assumptions.length) {
            console.log('ADD PREMISE');
            let newButton = createFormulaButton(
                root,
                treeNode.right.assumptions.last().formulaTree, 
                removebrackets(traverse(treeNode.right.assumptions.last().formulaTree)), 
                treeNode.right.assumptions.last().uniqId,
                treeNode.right,
                treeNode.right.assumptions.last().reason 
            );

            insertAfter(
                newButton,
                document.getElementById(treeNode.assumptions.last().uniqId).parentNode.parentNode
            );

            renderNode(root,treeNode.right);
        } else if (treeNode.conclusion.uniqId != treeNode.right.conclusion.uniqId) {
            //insert before uniq id
            console.log('ADD CONCLUSION');
                let space = createFormulaButton(root,
                    null,
                    '...',
                    null,
                    treeNode.right
                );
            
            let newConclusion = createFormulaButton(
                root,
                treeNode.right.conclusion.formulaTree,
                removebrackets(traverse(treeNode.right.conclusion.formulaTree)),
                treeNode.right.conclusion.uniqId,
                treeNode.right,
                treeNode.right.conclusion.reason
            );
            
            if(treeNode.right.conclusion.reason==null){
            document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode
                .insertBefore(
                space,
                document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode
            )
                }
            
            document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode.parentNode
                .insertBefore(
                newConclusion,
                document.getElementById(treeNode.conclusion.uniqId).parentNode.parentNode
            )
            
            
            renderNode(root,treeNode.right);
               };
    };
            
        };
function isSolved(tree) {
    let allNodes = getAllNodes(tree, []);
    let LastOccurrenceOfConclusion;
    for (let i = 0; i < allNodes.length; i++) {
        if (allNodes[i].conclusion.reason == null) {
            return false;
        }
    }
    
    for (let i = 0; i < allProofs.length; i++) {
        if (JSON.stringify(tree) == JSON.stringify(allProofs[i].tree)) {
            allProofs[i].isSolved = true;
            refreshProofs();
        }
    }
}







function getReason() {

    let allBadges = document.getElementsByClassName('badge-pill');
    let allLis = document.getElementsByClassName('li');


    for (let badge of allBadges) {
            let reasonPresentation;

        if (badge.textContent.split('_')[1]) {
            for (let i = 0; i < allLis.length; i++) {

                if (badge.textContent.split('_')[1] == allLis[i].id) {
                    let correctNumber = i + 1;
                    reasonPresentation = badge.textContent.split('_')[0] + ' ' + correctNumber;
                }
                

                if (badge.textContent.split('_')[2] == allLis[i].id) {
                    let correctNumber = i + 1;
                    reasonPresentation += '-' + correctNumber;
                }
            }
                   badge.innerHTML = reasonPresentation
        } else{
            badge.innerHTML = badge.textContent.split('_')[0];
        }

    }   
 return true;


/*
        if (badge.textContent.split('_')[1]) {
            for (let i = 0; i < allLis.length; i++) {

                if (badge.textContent.split('_')[1] == allLis[i].id) {
                    let correctNumber = i + 1;
                    badge.innerHTML = badge.textContent.split('_')[0] + ' ' + correctNumber;
                }
            }
        }
*/
    

}
function createFormulaButton(treeNode, formulaTree, presentation, uniqId, formulaNode,reason) {
    //treenode=root
    let boxLength;
    if (formulaNode.box) {
        boxLength = formulaNode.box.length;
    } else {
        boxLength = -1;
    }

    let li = document.createElement('li');
    let div = document.createElement('div');
    let a = document.createElement('a');
    let span = document.createElement('span');
    let badge = document.createElement('span');
    div.className = "tooltips";
    div.classList.add("margin"+ boxLength);
    a.className = "list-group-item li";
    span.className = "tooltiptext";
    badge.className = "badge badge-pill badge-primary";
    li.appendChild(div);
    div.appendChild(a);
    div.appendChild(span);
    a.setAttribute("id", uniqId);
    a.setAttribute('href', '#');
    a.innerHTML = presentation;

    
   if(reason){
       badge.innerHTML=reason;    
           a.appendChild(badge);
   }


    a.addEventListener('click', function (e) {
        changeIsClicked(a);
    });
    
    
    
    let kind = first(formulaTree);
    
    
    
                let classical_conta = document.createElement('a');
            classical_conta.className = "b list-group-item";
            classical_conta.setAttribute('href', '#');
            classical_conta.innerHTML = "Classic Contra";
            span.appendChild(classical_conta);
            classical_conta.addEventListener('click', function () {
                classical_conta_rule(treeNode, getClicked());
            });
    
    
        let conjunction_intro = document.createElement('a');
            conjunction_intro.className = "b list-group-item";
            conjunction_intro.setAttribute('href', '#');
            conjunction_intro.innerHTML = "∧ intro";
            span.appendChild(conjunction_intro);
            conjunction_intro.addEventListener('click', function () {
                conjunction_intro_rule(treeNode, getClicked())
            });
    
            let conjunction_elim_left = document.createElement('a');
            conjunction_elim_left.className = "b list-group-item";
            conjunction_elim_left.setAttribute('href', '#');
            conjunction_elim_left.innerHTML = "∧ Elim (left)";
            span.appendChild(conjunction_elim_left);
            conjunction_elim_left.addEventListener('click', function () {
                conjunction_elim_left_rule(treeNode, getClicked())
            });

            let conjunction_elim_right = document.createElement('a');
            conjunction_elim_right.className = "b list-group-item";
            conjunction_elim_right.setAttribute('href', '#');
            conjunction_elim_right.innerHTML = "∧ Elim (right)";
            span.appendChild(conjunction_elim_right);
            conjunction_elim_right.addEventListener('click', function () {
                conjunction_elim_right_rule(treeNode, getClicked())
            });
                let implies_intro = document.createElement('a');
            implies_intro.className = "b list-group-item";
            implies_intro.setAttribute('href', '#');
            implies_intro.innerHTML = "→ Intro";
            span.appendChild(implies_intro);
            implies_intro.addEventListener('click', function () {
                implies_intro_rule(treeNode, getClicked());
            });
            
            
            let implies_elim = document.createElement('a');
            implies_elim.className = "b list-group-item";
            implies_elim.setAttribute('href', '#');
            implies_elim.innerHTML = "→ Elim";
            span.appendChild(implies_elim);
            implies_elim.addEventListener('click', function () {
                implies_elim_rule(treeNode, getClicked());
            });
            
            
            let negation_elim = document.createElement('a');
            negation_elim.className = "b list-group-item";
            negation_elim.setAttribute('href', '#');
            negation_elim.innerHTML = "¬ Elim";
            span.appendChild(negation_elim);
            negation_elim.addEventListener('click', function () {
                negation_elim_rule(treeNode, getClicked());
            });
    
            let doublenegation_elim = document.createElement('a');
            doublenegation_elim.className = "b list-group-item";
            doublenegation_elim.setAttribute('href', '#');
            doublenegation_elim.innerHTML = "¬¬ Elim";
            span.appendChild(doublenegation_elim);
            doublenegation_elim.addEventListener('click', function () {
                doublenegation_elim_rule(treeNode, getClicked());
            });

            let negation_intro = document.createElement('a');
            negation_intro.className = "b list-group-item";
            negation_intro.setAttribute('href', '#');
            negation_intro.innerHTML = "¬ Intro";
            span.appendChild(negation_intro);
            negation_intro.addEventListener('click', function () {
                negation_intro_rule(treeNode, getClicked());
            });
          
            
                 let alternative_elim = document.createElement('a');
            alternative_elim.className = "b list-group-item";
            alternative_elim.setAttribute('href', '#');
            alternative_elim.innerHTML = "∨ Elim";
            span.appendChild(alternative_elim);
            alternative_elim.addEventListener('click', function () {
                alternative_elim_rule(treeNode, getClicked());
            });
            
            let alternative_intro_left = document.createElement('a');
            alternative_intro_left.className = "b list-group-item";
            alternative_intro_left.setAttribute('href', '#');
            alternative_intro_left.innerHTML = "∨ Intro (left)";
            span.appendChild(alternative_intro_left);
            alternative_intro_left.addEventListener('click', function () {
                alternative_intro_left_rule(treeNode, getClicked());
            });
            
            let alternative_intro_right = document.createElement('a');
            alternative_intro_right.className = "b list-group-item";
            alternative_intro_right.setAttribute('href', '#');
            alternative_intro_right.innerHTML = "∨ Intro (right)";
            span.appendChild(alternative_intro_right);
            alternative_intro_right.addEventListener('click', function () {
                alternative_intro_right_rule(treeNode, getClicked());
            });
                let contradiction_elim = document.createElement('a');
            contradiction_elim.className = "b list-group-item";
            contradiction_elim.setAttribute('href', '#');
            contradiction_elim.innerHTML = "⊥ Elim";
            span.appendChild(contradiction_elim);
            contradiction_elim.addEventListener('click', function () {
                contradiction_elim_rule(treeNode, getClicked());
            });

            
    switch (kind) {
        case 'and':

            break;
        case 'implies':

        case 'or':

        case 'not':
          /*  let negation_elim = document.createElement('a');
            negation_elim.className = "b list-group-item";
            negation_elim.setAttribute('href', '#');
            negation_elim.innerHTML = "negation elim";
            span.appendChild(negation_elim);
            negation_elim.addEventListener('click', function () {
                negation_elim_rule(treeNode, getClicked());
            });

            let negation_intro = document.createElement('a');
            negation_intro.className = "b list-group-item";
            negation_intro.setAttribute('href', '#');
            negation_intro.innerHTML = "negation intro";
            span.appendChild(negation_intro);
            negation_intro.addEventListener('click', function () {
                negation_intro_rule(treeNode, getClicked());
            });
            break;
            */
            case 'contradiction':

            break;  

    }
    return li;
}
function addPremiseForAll(root, premiseSourceNode, premise) {
    let arrayWithNodes = getAllNodes(root, []);
    let readyToAdd = false;

    for (let i = 0; i < arrayWithNodes.length; i++) {
        if (readyToAdd && premiseSourceNode.box == null) {
            arrayWithNodes[i].assumptions.unshift(premise);


            if (JSON.stringify(premise.formulaTree) == JSON.stringify(arrayWithNodes[i].conclusion.formulaTree)) {
                arrayWithNodes[i].conclusion.reason = 'hyp_' + premise.uniqId;
            }



        } else if (readyToAdd && (arrayWithNodes[i].box == premiseSourceNode.box || arrayWithNodes[i].startsWith(premiseSourceNode.box))) {
            arrayWithNodes[i].assumptions.unshift(premise);

            if (JSON.stringify(premise.formulaTree) == JSON.stringify(arrayWithNodes[i].conclusion)) {
                arrayWithNodes[i].conclusion.reason = 'hyp_' + premise.uniqId;
            }




        }
        if (arrayWithNodes[i] == premiseSourceNode) {
            readyToAdd = true;
        }
    }
};


function findConclusionReason(treeNode) {
    for (let i = 0; i < treeNode.assumptions.length; i++) {
        if (JSON.stringify(treeNode.assumptions[i].formulaTree) == JSON.stringify(treeNode.conclusion.formulaTree)) {
            treeNode.conclusion.reason = 'hyp_' + treeNode.assumptions[i].uniqId;
            return true;
        }
    }
    return false;
}


function isNeighborIsReason(treeNode) {
    console.log('asd');
    console.log(JSON.stringify(treeNode.assumptions[treeNode.assumptions.length - 1].formulaTree) +'=='+ JSON.stringify(treeNode.conclusion.formulaTree)) 
    if (JSON.stringify(treeNode.assumptions[treeNode.assumptions.length - 1].formulaTree) == JSON.stringify(treeNode.conclusion.formulaTree)) {
return true;
    }
    return false;
}

function findLastOccurrenceOfConclusion(tree, getClickedUniqId) { 

    let allNodes = getAllNodes(tree,[]);
    let LastOccurrenceOfConclusion;
    for(let i=0;i<allNodes.length;i++){
        if(allNodes[i].conclusion.uniqId==getClickedUniqId){
            LastOccurrenceOfConclusion = allNodes[i];
        }
    }
    
    return LastOccurrenceOfConclusion;
    
};
function findFirstOccurrenceOfPremise(tree, getClickedUniqId) {
        let allNodes = getAllNodes(tree,[]);
        let FirstOccurrenceOfPremiseNode;
        for (let i = 0; i < allNodes.length; i++) {
            for (let j = 0; j < allNodes[i].assumptions.length; j++) {
                if (allNodes[i].assumptions[j].uniqId == getClickedUniqId) {
                    FirstOccurrenceOfPremiseNode = allNodes[i];
                    return FirstOccurrenceOfPremiseNode;
                }
            }
        }  
};
function findLastOccurrenceOfPremise(tree, getClickedUniqId) {
        let allNodes = getAllNodes(tree,[]);
    let LastOccurrenceOfPremiseNode;
    
    
    
        for (let i = 0; i < allNodes.length; i++) {
            for (let j = 0; j < allNodes[i].assumptions.length; j++) {
                if (allNodes[i].assumptions[j].uniqId == getClickedUniqId) {
                    LastOccurrenceOfPremiseNode = allNodes[i];    
                }
            }
        }  
    return LastOccurrenceOfPremiseNode;
}
function getFormulaTreeOfClicked(treeNode, searchedUniqId) {
    let allNodes = getAllNodes(treeNode, [])

    for (let j = 0; j < allNodes.length; j++) {
        console.log(j)
        console.log(allNodes[j].conclusion.uniqId+'==' + searchedUniqId)
        
            if (allNodes[j].conclusion.uniqId == searchedUniqId) {
                console.log(allNodes[j].conclusion.uniqId+'==' + searchedUniqId)
                return allNodes[j].conclusion.formulaTree;
            }
        
        
        
        
        for (let i = 0; i < allNodes[j].assumptions.length; i++) {
            console.log('i='+i)
            if (allNodes[j].assumptions[i].uniqId == searchedUniqId) {
                console.log('ok')
                return allNodes[j].assumptions[i].formulaTree;
            }


        }
    }
}
function getAllNodes(root, arrayWithNodes) {
    arrayWithNodes.push(root);

    if (root.left) {
         getAllNodes(root.left, arrayWithNodes);
    }
    if (root.right) {
         getAllNodes(root.right, arrayWithNodes);
    }
    return arrayWithNodes;
};
function lookForFreeBox(root,currentBox){
    if(currentBox == null){
        for(let i =1; i<10; i++){
            if(isBoxFree(root,i.toString()) != false){
                return i.toString();
            }
        }
        
    } else {
        let splitedCurrentBox; 
        if(currentBox.toString().length == 1)
            splitedCurrentBox = [currentBox];
        else{
            splitedCurrentBox = currentBox.split('.').map(Number);
        }
        
        for(let i =1; i<10; i++){
            let searchedBox = splitedCurrentBox.slice();
            searchedBox.push(i);
            if(isBoxFree(root,searchedBox.join('.'))){
                return searchedBox.join('.')
            }
        }
    }
};
function isBoxFree(root,searchedBox){

    if(root.box===searchedBox){
        return false;
    }
    
      if (root.left) {
          return isBoxFree(root.left,searchedBox);
      }

      if (root.right) {
          return isBoxFree(root.right,searchedBox);
      }
    return true;
};
function insertLeftNodeBetween(node,newLeft){
    let oldLeft = node.left;
    node.left=newLeft; 
    newLeft.left=oldLeft;
}
function insertRightNodeBetween(node,newRight){
    let oldRight = node.right;
    node.right=newRight; 
    newRight.right=oldRight;
}








//RULES

function conjunction_intro_rule(tree, getClickedUniqIds) {
        let clickedFormula1 = getClickedUniqIds[0];
        let clickedFormula2 = getClickedUniqIds[1];

        let clickedFormula1Node = findFirstOccurrenceOfPremise(tree, clickedFormula1);
        let clickedFormula2Node = findFirstOccurrenceOfPremise(tree, clickedFormula2);

        let formula1Tree = getFormulaTreeOfClicked(clickedFormula1Node, clickedFormula1);
        let formula2Tree = getFormulaTreeOfClicked(clickedFormula2Node, clickedFormula2);


        let newPremiseFormula = {
        and: {left: formula1Tree, right:formula2Tree }
        };
    
        //nowa wynik reguly (lewa formula koniunkcji)
        let newFormula = new formula(newPremiseFormula , generateUniqId(), 'conjuction intro_' + clickedFormula1 + '_' + clickedFormula2)

        //kopia przeslanek ojca
        let arrayWithAddedPremise = clickedFormula2Node.assumptions.slice();

        //dodanie nowej przeslanki do przeslanek nowego wezla
        arrayWithAddedPremise.push(newFormula);

            if(JSON.stringify(newFormula.formulaTree) == JSON.stringify(clickedFormula2Node.conclusion.formulaTree)){
                clickedFormula2Node.conclusion.reason = 'conjuction intro_' + getClickedUniqIds;
                
            //aktualizacja drzewa
            createTool(tree);

            //zakończenie pracy funkcji
            return true;
    }
        
        //utworzenie nowego wezla
        let newNode = new treeNode(arrayWithAddedPremise, clickedFormula2Node.conclusion);

        //wprowadzenie wezla do drzewa
        insertLeftNodeBetween(clickedFormula2Node, newNode);

        //rozdanie przeslanki wezlom ponizej
        addPremiseForAll(tree, clickedFormula2Node.left, newFormula);

        //aktualizacja drzewa
        createTool(tree);

        //zakończenie pracy funkcji
        return true;

};


function conjunction_elim_left_rule(tree, getClickedUniqIds) {

    let FirstOccurrenceOfPremiseNode = findFirstOccurrenceOfPremise(tree,getClickedUniqIds);

    let FormulaTreeOfClicked = getFormulaTreeOfClicked(FirstOccurrenceOfPremiseNode,getClickedUniqIds);
    
    //nowa wynik reguly (lewa formula koniunkcji)
    let newFormula = new formula(FormulaTreeOfClicked.and.left, generateUniqId(), '∧ elim_' + getClickedUniqIds)

    //jeżeli konkluzja jest wynikiem zastosowania reguły to nie tworzymy nowego wezla
    if(JSON.stringify(newFormula.formulaTree) == JSON.stringify(FirstOccurrenceOfPremiseNode.conclusion.formulaTree)){
                FirstOccurrenceOfPremiseNode.conclusion.reason = '∧ elim_' + getClickedUniqIds;
                
            //aktualizacja drzewa
            createTool(tree);

            //zakończenie pracy funkcji
            return true;
    }
    
    //kopia przeslanek ojca
    let arrayWithAddedPremise = FirstOccurrenceOfPremiseNode.assumptions.slice();

    //dodanie nowej przeslanki do przeslanek nowego wezla
    arrayWithAddedPremise.push(newFormula);

    //utworzenie nowego wezla
    let newNode = new treeNode(arrayWithAddedPremise, FirstOccurrenceOfPremiseNode.conclusion);

    //wprowadzenie wezla do drzewa
    insertLeftNodeBetween(FirstOccurrenceOfPremiseNode, newNode);

    //rozdanie przeslanki wezlom ponizej
    addPremiseForAll(tree,FirstOccurrenceOfPremiseNode.left,newFormula);

    //aktualizacja drzewa
    createTool(tree);

    //zakończenie pracy funkcji
    return true;
};
function conjunction_elim_right_rule(tree, getClickedUniqIds) {

    
    let FirstOccurrenceOfPremiseNode = findFirstOccurrenceOfPremise(tree,getClickedUniqIds);
    
    let FormulaTreeOfClicked = getFormulaTreeOfClicked(FirstOccurrenceOfPremiseNode,getClickedUniqIds);

    //nowa wynik reguly (lewa formula koniunkcji)
    let newFormula = new formula(FormulaTreeOfClicked.and.right, generateUniqId(), '∧ elim_' + getClickedUniqIds)
    
        //jeżeli konkluzja jest wynikiem zastosowania reguły to nie tworzymy nowego wezla
    if(JSON.stringify(newFormula.formulaTree) == JSON.stringify(FirstOccurrenceOfPremiseNode.conclusion.formulaTree)){
                FirstOccurrenceOfPremiseNode.conclusion.reason = '∧ elim_' + getClickedUniqIds;
                
            //aktualizacja drzewa
            createTool(tree);

            //zakończenie pracy funkcji
            return true;
    }
    
    //kopia przeslanek ojca
    let arrayWithAddedPremise = FirstOccurrenceOfPremiseNode.assumptions.slice();

    //dodanie nowej przeslanki do przeslanek nowego wezla
    arrayWithAddedPremise.push(newFormula);

    //utworzenie nowego wezla
    let newNode = new treeNode(arrayWithAddedPremise, FirstOccurrenceOfPremiseNode.conclusion);

    //wprowadzenie wezla do drzewa
    insertLeftNodeBetween(FirstOccurrenceOfPremiseNode, newNode);

    //rozdanie przeslanki wezlom ponizej
    addPremiseForAll(tree,FirstOccurrenceOfPremiseNode.left,newFormula);

    //aktualizacja drzewa
    createTool(tree);

    //zakończenie pracy funkcji
    return true;
};
function implies_intro_rule(tree, getClickedUniqIds) {
    let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree,getClickedUniqIds);
    let FormulaTreeOfClicked = getFormulaTreeOfClicked(tree, getClickedUniqIds);
    let newPremise = new formula(FormulaTreeOfClicked.implies.left, generateUniqId(), 'assumption')
    let newConclusion = new formula(FormulaTreeOfClicked.implies.right, generateUniqId())
    let copyOfPremises = LastOccurrenceOfConclusion.assumptions.slice();
    copyOfPremises.push(newPremise);
    //utworzenie nowego wezla w nowym pudelku
    let newNode = new treeNode(copyOfPremises, newConclusion, lookForFreeBox(tree, LastOccurrenceOfConclusion.box));
    

    //doczepienie wezla do drzewa
    insertLeftNodeBetween(LastOccurrenceOfConclusion, newNode);
    //rozdanie przeslanki wezlom ponizej

    if (LastOccurrenceOfConclusion.left.left) {
        addPremiseForAll(tree, LastOccurrenceOfConclusion.left, newPremise);
    }
    
    findConclusionReason(newNode);


    LastOccurrenceOfConclusion.conclusion.reason = 'implies intro_'+newPremise.uniqId+'_'+newConclusion.uniqId;
    console.log('treefromrule\n'+JSON.stringify(tree))
    createTool(tree);
    return true;
};
function implies_elim_rule(tree, getClickedUniqIds) {
    //2 przeslanki i ich uniqIdsy
    let clickedFormula1 = getClickedUniqIds[0];
    let clickedFormula2 = getClickedUniqIds[1];



    //jezeli są dwie przesłanki
    if (findFirstOccurrenceOfPremise(tree, clickedFormula1) && findFirstOccurrenceOfPremise(tree, clickedFormula2)) {

        let clickedFormula1Node = findFirstOccurrenceOfPremise(tree, clickedFormula1);
        let formula1Tree = getFormulaTreeOfClicked(clickedFormula1Node, clickedFormula1);
        let clickedFormula2Node = findFirstOccurrenceOfPremise(tree, clickedFormula2);
        let formula2Tree = getFormulaTreeOfClicked(clickedFormula2Node, clickedFormula2);

        //pierwsza formula jest implikacją, a lewa strona imlplikacji jest taka sama jak druga zaznaczona formula
        if (formula1Tree.implies && (JSON.stringify(formula1Tree.implies.left) == JSON.stringify(formula2Tree))) {



            //nowa formula - wynik reguly (prawa formula implikacji)
            let newFormula = new formula(formula1Tree.implies.right, generateUniqId(), 'implies elim_' + clickedFormula1 + '_' + clickedFormula2);


            //jeżeli wynik reguły jest szukanym twierdzeniem to zamiast tworzyc nowy wezel, uzupelniamy uzasadnienie 
            if (JSON.stringify(newFormula.formulaTree) == JSON.stringify(clickedFormula2Node.conclusion.formulaTree)) {
                clickedFormula2Node.conclusion.reason = 'implies elim_' + clickedFormula1 + '_' + clickedFormula2;

                //aktualizacja drzewa
                createTool(tree);

                //zakończenie pracy funkcji
                return true;
            } else {

                //kopia przeslanek ojca
                let arrayWithAddedPremise = clickedFormula2Node.assumptions.slice();

                //dodanie nowej przeslanki do przeslanek nowego wezla
                arrayWithAddedPremise.push(newFormula);

                //utworzenie nowego wezla
                let newNode = new treeNode(arrayWithAddedPremise, clickedFormula2Node.conclusion, clickedFormula2Node.box);

                //wprowadzenie wezla do drzewa
                insertLeftNodeBetween(clickedFormula2Node, newNode);

                //rozdanie przeslanki wezlom ponizej
                addPremiseForAll(tree, clickedFormula2Node.left, newFormula);

                //aktualizacja drzewa
                createTool(tree);

                //zakończenie pracy funkcji
                return true;
            }

        } else if (formula2Tree.implies && (JSON.stringify(formula2Tree.implies.left) == JSON.stringify(formula1Tree))) {

            //nowa wynik reguly (lewa formula koniunkcji)
            let newFormula = new formula(formula2Tree.implies.right, generateUniqId(), 'implies elim_' + clickedFormula1 + '_' + clickedFormula2)

            if (JSON.stringify(newFormula.formulaTree) == JSON.stringify(clickedFormula2Node.conclusion.formulaTree)) {
                clickedFormula2Node.conclusion.reason = 'implies elim_' + clickedFormula1 + '_' + clickedFormula2;

                //aktualizacja drzewa
                createTool(tree);

                //zakończenie pracy funkcji
                return true;
            } else {
                //kopia przeslanek ojca
                let arrayWithAddedPremise = clickedFormula2Node.assumptions.slice();

                //dodanie nowej przeslanki do przeslanek nowego wezla
                arrayWithAddedPremise.push(newFormula);

                //utworzenie nowego wezla
                let newNode = new treeNode(arrayWithAddedPremise, clickedFormula2Node.conclusion, clickedFormula2Node.box);


                //wprowadzenie wezla do drzewa
                insertLeftNodeBetween(clickedFormula2Node, newNode);

                //rozdanie przeslanki wezlom ponizej
                addPremiseForAll(tree, clickedFormula2Node.left, newFormula);

                //aktualizacja drzewa
                createTool(tree);

                //zakończenie pracy funkcji
                return true;
            }

        }





        //przeslanka i konkluzja
    } else if (findFirstOccurrenceOfPremise(tree, clickedFormula1) && findLastOccurrenceOfConclusion(tree, clickedFormula2)) {

        let clickedFormula1Node = findFirstOccurrenceOfPremise(tree, clickedFormula1);
        let formula1Tree = getFormulaTreeOfClicked(clickedFormula1Node, clickedFormula1);
        let clickedFormula2Node = findLastOccurrenceOfConclusion(tree, clickedFormula2);
        let formula2Tree = clickedFormula2Node.conclusion.formulaTree;


        if (formula1Tree.implies && (JSON.stringify(formula1Tree.implies.right) == JSON.stringify(formula2Tree))) {
            let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree, clickedFormula2);
            let copyOfPremisses = LastOccurrenceOfConclusion.assumptions.slice();
            let newConclusionFormula = new formula(formula1Tree.implies.left, generateUniqId());

            let newNode = new treeNode(copyOfPremisses, newConclusionFormula, LastOccurrenceOfConclusion.box);

            if (isNeighborIsReason(newNode)) {
                LastOccurrenceOfConclusion.conclusion.reason = 'implies elim_' + clickedFormula1 + '_' + clickedFormula2;
                createTool(tree);
                return true;
            } else {

                LastOccurrenceOfConclusion.conclusion.reason = 'implies elim_' + clickedFormula1 + '_' + newConclusionFormula.uniqId;
                findConclusionReason(newNode);
                insertLeftNodeBetween(LastOccurrenceOfConclusion, newNode);
                createTool(tree);
                return true;
            }
        }
    }
};
function alternative_elim_rule(tree, getClickedUniqIds) {

    let clickedPremise = getClickedUniqIds[0];
    let clickedConclusion = getClickedUniqIds[1];

    let clickedPremiseNode = findFirstOccurrenceOfPremise(tree, clickedPremise);
    let clickedConclusionNode = findLastOccurrenceOfConclusion(tree,clickedConclusion);

    let formulaOfAlternative = getFormulaTreeOfClicked(clickedPremiseNode, clickedPremise);
    let formulaOfConclusion = getFormulaTreeOfClicked(clickedConclusionNode, clickedConclusion);

    let newAssumption1 = new formula(formulaOfAlternative.or.left, generateUniqId(), 'assumption');
    let copyOfPremisses1 = clickedConclusionNode.assumptions.slice();
    copyOfPremisses1.push(newAssumption1);
    let newConclusion1 = new formula(formulaOfConclusion, generateUniqId());



    let newLeftNode = new treeNode(copyOfPremisses1, newConclusion1,lookForFreeBox(tree,clickedConclusionNode.box));
    
        insertLeftNodeBetween(clickedConclusionNode, newLeftNode);
    
    
    let newAssumption2 = new formula(formulaOfAlternative.or.right, generateUniqId(), 'assumption');
    let copyOfPremisses2 = clickedConclusionNode.assumptions.slice();
    copyOfPremisses2.push(newAssumption2);
    let newConclusion2 = new formula(formulaOfConclusion, generateUniqId());
    let newRightNode = new treeNode(copyOfPremisses2, newConclusion2,lookForFreeBox(tree,clickedConclusionNode.box));
    insertRightNodeBetween(clickedConclusionNode, newRightNode);
    
        clickedConclusionNode.conclusion.reason = 'alternative intro_' + newConclusion1.uniqId + '_' + newConclusion2.uniqId;
    
    findConclusionReason(newLeftNode);
    findConclusionReason(newRightNode);
    //aktualizacja drzewa
    createTool(tree);

    //zakończenie pracy funkcji
    return true;
};
function alternative_intro_left_rule(tree, getClickedUniqIds) {
    let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree, getClickedUniqIds);
    let formulaOfConclusion = getFormulaTreeOfClicked(LastOccurrenceOfConclusion, getClickedUniqIds);
    let newConclusionFormula = new formula(formulaOfConclusion.or.left, generateUniqId());
    let copyOfPremisses = LastOccurrenceOfConclusion.assumptions.slice();
    let newNode = new treeNode(copyOfPremisses, newConclusionFormula,LastOccurrenceOfConclusion.box);
    LastOccurrenceOfConclusion.conclusion.reason = 'alternate intro ' + newConclusionFormula.uniqId
    
        if (isNeighborIsReason(newNode)) {
                LastOccurrenceOfConclusion.conclusion.reason = 'alternate intro_' + LastOccurrenceOfConclusion.assumptions[LastOccurrenceOfConclusion.assumptions.length-1].uniqId
                createTool(tree);
                return true;
            } else {
    
    findConclusionReason(newNode);
    insertLeftNodeBetween(LastOccurrenceOfConclusion,newNode);

    createTool(tree);
    return true;}
};
function alternative_intro_right_rule(tree, getClickedUniqIds) {
    let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree, getClickedUniqIds);
    let formulaOfConclusion = getFormulaTreeOfClicked(LastOccurrenceOfConclusion, getClickedUniqIds);
    let newConclusionFormula = new formula(formulaOfConclusion.or.right, generateUniqId());
    let copyOfPremisses = LastOccurrenceOfConclusion.assumptions.slice();
    let newNode = new treeNode(copyOfPremisses, newConclusionFormula,LastOccurrenceOfConclusion.box);
    LastOccurrenceOfConclusion.conclusion.reason = 'alternate intro ' + newConclusionFormula.uniqId
    
    
            if (isNeighborIsReason(newNode)) {
                LastOccurrenceOfConclusion.conclusion.reason = 'alternate intro_' + LastOccurrenceOfConclusion.assumptions[LastOccurrenceOfConclusion.assumptions.length-1].uniqId
                createTool(tree);
                return true;
            } else {
    
    findConclusionReason(newNode);
    insertLeftNodeBetween(LastOccurrenceOfConclusion,newNode);
    createTool(tree);
    return true;
            }
};
function classical_conta_rule(tree, getClickedUniqIds) {
    let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree, getClickedUniqIds);

    let FormulaTreeOfClicked = getFormulaTreeOfClicked(tree, getClickedUniqIds);
    
console.log('conta');
    console.log(LastOccurrenceOfConclusion);
    console.log(FormulaTreeOfClicked);
    


    let newPremiseFormula = {
        not: FormulaTreeOfClicked
    };
    let newPremise = new formula(newPremiseFormula, generateUniqId(), 'assumption')
    let newConclusion = new formula(contradiction, generateUniqId())

    let copyOfPremises = LastOccurrenceOfConclusion.assumptions.slice();
    copyOfPremises.push(newPremise);

    //utworzenie nowego wezla w nowym pudelku
    let newNode = new treeNode(copyOfPremises, newConclusion, lookForFreeBox(tree, LastOccurrenceOfConclusion.box));

    //doczepienie wezla do drzewa
    insertLeftNodeBetween(LastOccurrenceOfConclusion, newNode);
    //rozdanie przeslanki wezlom ponizej

    if (LastOccurrenceOfConclusion.left.left) {
        addPremiseForAll(tree, LastOccurrenceOfConclusion.left, newPremise);
    }
    
    
            LastOccurrenceOfConclusion.conclusion.reason = 'Classical Contra_'+ newPremise.uniqId+'_'+newConclusion.uniqId;
    
    createTool(tree);
    return true;
};
function negation_elim_rule(tree, getClickedUniqIds) {

    if (getClickedUniqIds.length == 1) {
        console.log('getClickedUniqIds.length ' + getClickedUniqIds.length)
        let FirstOccurrenceOfPremiseNode = findFirstOccurrenceOfPremise(tree, getClickedUniqIds);

        let FormulaTreeOfClicked = getFormulaTreeOfClicked(tree, getClickedUniqIds);
        let newConclusionFormula = new formula(FormulaTreeOfClicked.not, generateUniqId());


        let copyOfPremisses = FirstOccurrenceOfPremiseNode.assumptions.slice();
        let newLeftNode = new treeNode(copyOfPremisses, newConclusionFormula, FirstOccurrenceOfPremiseNode.box);

        FirstOccurrenceOfPremiseNode.conclusion.reason = 'negation elim'
        insertLeftNodeBetween(FirstOccurrenceOfPremiseNode, newLeftNode);

        createTool(tree);
        return true;
    } else if ((getClickedUniqIds.length == 2)) {
        console.log('getClickedUniqIds.length ' + getClickedUniqIds.length)
        let clickedFormula1 = getClickedUniqIds[0];
        let clickedFormula2 = getClickedUniqIds[1];

        let clickedFormula1Node = findFirstOccurrenceOfPremise(tree, clickedFormula1);
        let clickedFormula2Node = findFirstOccurrenceOfPremise(tree, clickedFormula2);

        let formula1Tree = getFormulaTreeOfClicked(clickedFormula1Node, clickedFormula1);
        let formula2Tree = getFormulaTreeOfClicked(clickedFormula2Node, clickedFormula2);



        //nowa wynik reguly (lewa formula koniunkcji)
        let newFormula = new formula(contradiction, generateUniqId(), 'negation elim_' + clickedFormula1 + '_' + clickedFormula2)

        //kopia przeslanek ojca
        let arrayWithAddedPremise = clickedFormula2Node.assumptions.slice();

        //dodanie nowej przeslanki do przeslanek nowego wezla
        arrayWithAddedPremise.push(newFormula);

            if(JSON.stringify(newFormula.formulaTree) == JSON.stringify(clickedFormula2Node.conclusion.formulaTree)){
                clickedFormula2Node.conclusion.reason = 'negation elim_' + getClickedUniqIds;
                
            //aktualizacja drzewa
            createTool(tree);

            //zakończenie pracy funkcji
            return true;
    }
        
        //utworzenie nowego wezla
        let newNode = new treeNode(arrayWithAddedPremise, clickedFormula2Node.conclusion);

        //wprowadzenie wezla do drzewa
        insertLeftNodeBetween(clickedFormula2Node, newNode);

        //rozdanie przeslanki wezlom ponizej
        addPremiseForAll(tree, clickedFormula2Node.left, newFormula);

        //aktualizacja drzewa
        createTool(tree);

        //zakończenie pracy funkcji
        return true;


    }
};
function negation_intro_rule(tree,getClickedUniqIds){
    let LastOccurrenceOfConclusion = findLastOccurrenceOfConclusion(tree, getClickedUniqIds);
    let FormulaTreeOfClicked = getFormulaTreeOfClicked(tree, getClickedUniqIds);
    


    let newPremiseFormula = FormulaTreeOfClicked.not;
    
    let newPremise = new formula(newPremiseFormula, generateUniqId(), 'assumption')
    let newConclusion = new formula(contradiction, generateUniqId())

    let copyOfPremises = LastOccurrenceOfConclusion.assumptions.slice();
    copyOfPremises.push(newPremise);

    //utworzenie nowego wezla w nowym pudelku
    let newNode = new treeNode(copyOfPremises, newConclusion, lookForFreeBox(tree, LastOccurrenceOfConclusion.box));

    //doczepienie wezla do drzewa
    insertLeftNodeBetween(LastOccurrenceOfConclusion, newNode);
    //rozdanie przeslanki wezlom ponizej

    if (LastOccurrenceOfConclusion.left.left) {
        addPremiseForAll(tree, LastOccurrenceOfConclusion.left, newPremise);
    }
    
    
    LastOccurrenceOfConclusion.conclusion.reason = 'negation intro_'+ newPremise.uniqId+'_'+newConclusion.uniqId;
    
    createTool(tree);
    return true;
};
function contradiction_elim_rule(tree,getClickedUniqIds){
    let FirstOccurrenceOfPremiseNode = findFirstOccurrenceOfPremise(tree, getClickedUniqIds);


    FirstOccurrenceOfPremiseNode.conclusion.reason = 'contradiction elim_' + getClickedUniqIds;
    //aktualizacja drzewa
    createTool(tree);

    //zakończenie pracy funkcji
    return true;
}
function doublenegation_elim_rule(tree,getClickedUniqIds){
    let FirstOccurrenceOfPremise = findFirstOccurrenceOfPremise(tree, getClickedUniqIds);
    let FormulaTreeOfClicked = getFormulaTreeOfClicked(tree, getClickedUniqIds);
    let newPremiseFormula = FormulaTreeOfClicked.not.not;
    
    let newPremise = new formula(newPremiseFormula, generateUniqId(), 'doublenegationelim')

    let copyOfPremises = FirstOccurrenceOfPremise.assumptions.slice();
    copyOfPremises.push(newPremise);

    //utworzenie nowego wezla w nowym pudelku
    let newNode = new treeNode(copyOfPremises, FirstOccurrenceOfPremise.conclusion, lookForFreeBox(tree, FirstOccurrenceOfPremise.box));

    if(JSON.stringify(newPremise.formulaTree) == JSON.stringify(FirstOccurrenceOfPremise.conclusion.formulaTree)){
                FirstOccurrenceOfPremise.conclusion.reason = 'negneg elim_' + getClickedUniqIds;
                
            //aktualizacja drzewa
            createTool(tree);

            //zakończenie pracy funkcji
            return true;
    }
    
    //doczepienie wezla do drzewa
    insertLeftNodeBetween(FirstOccurrenceOfPremise, newNode);
    //rozdanie przeslanki wezlom ponizej

    if (FirstOccurrenceOfPremise.left.left) {
        addPremiseForAll(tree, FirstOccurrenceOfPremise.left, newPremise);
    }

    createTool(tree);
    return true;
}









//HELPERS

function getAssumptions(parsed_sequent_array) {
    let assumptions = [];
    for (let i = 0; i<parsed_sequent_array.length-1; i++) {
        let assumption = new formula(parsed_sequent_array[i],generateUniqId(),'assumption');
      assumptions.push(assumption);
    };

    return assumptions;
}; //pobieranie założeń                                
function getConclusion(parsed_sequent_array) {
    let conclusion = new formula(parsed_sequent_array.pop(),generateUniqId());
    return conclusion;
}; //pobieranie wniosku
function getPresentation(firstNode){
    let presentation = '';
    for(let i=0;i<firstNode.assumptions.length;i++){
       presentation += removebrackets(traverse(firstNode.assumptions[i].formulaTree));
        if(i != firstNode.assumptions.length-1){
             presentation += ', '
        }   
    };
    
    presentation += ' ⊢ ' + removebrackets(traverse(firstNode.conclusion.formulaTree))
    
    return presentation;
} 
function removebrackets(formula_to_remove_brackets) {
    return formula_to_remove_brackets.replace(/^\((.+)\)$/, '$1');
}
function traverse(obj) {
    var formula = '';
    
    if(obj.contradiction){
        formula = '⊥'
    }

    if (obj.not) {
        formula = formula.concat('¬', traverse(obj.not));
    }

    if (obj.implies) {
        formula = formula.concat('(', traverse(obj.implies.left), ' ⇒ ', traverse(obj.implies.right), ')');
    }

    if (obj.and) {
        formula = formula.concat('(', traverse(obj.and.left), ' ∧ ', traverse(obj.and.right), ')');
    }

    if (obj.or) {
        formula = formula.concat('(', traverse(obj.or.left), ' v ', traverse(obj.or.right), ')');
    }

    if (obj.predicate) {
        formula = formula.concat(formula, obj.predicate.name);
    }

    return formula;
}
function first(obj) {
    for (var a in obj) return a;
}
function changeIsClicked(formulaButton){
    if(formulaButton.className!="isClicked list-group-item"){
        formulaButton.className="isClicked list-group-item";
    } 
    else{
        formulaButton.className="list-group-item";
    } 
    
    return true;
}
function getClicked(){
    let isClicked = document.getElementsByClassName("isClicked");
    let isClickedIds = [];
    for (let item of isClicked) {
        isClickedIds.push(item.id);
    }
    return isClickedIds;
}
function JSONstringify(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, '\t');
    }
    var arr = [],
        _string = 'color:green',
        _number = 'color:darkorange',
        _boolean = 'color:blue',
        _null = 'color:magenta',
        _key = 'color:red';
    json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var style = _number;
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                style = _key;
            } else {
                style = _string;
            }
        } else if (/true|false/.test(match)) {
            style = _boolean;
        } else if (/null/.test(match)) {
            style = _null;
        }
        arr.push(style);
        arr.push('');
        return '%c' + match + '%c';
    });
    arr.unshift(json);
    console.log.apply(console, arr);
};
function generateUniqId() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}
Array.prototype.last = function() {
    return this[this.length - 1];
}
Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
};// => arr == [ 'A', 'B', 'C', 'D', 'E' ]



refreshProofs();
void(0);
