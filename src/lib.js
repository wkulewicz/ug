import pegjs from 'pegjs'
import parser from '../parser.js'
import parser2 from '../parser2.2.js'
import {findFirstOccurrenceOfPremise} from './app.js';

export class proof {
    constructor(presentation, tree, isSolved = false) {
        this.presentation = presentation;
        this.tree = tree;
        this.isSolved = isSolved;
    };
};

export class treeNode {
    constructor(assumptions, conclusion, box = null, left = null, right = null, isRoot = false) {
        this.assumptions = assumptions;
        this.conclusion = conclusion;
        this.box = box;
        this.left = left;
        this.right = right;
        this.isRoot = isRoot;
    };
};


export class formula {
    constructor(formulaTree, uniqId, reason = null) {
        this.formulaTree = formulaTree;
        this.uniqId = uniqId
        this.reason = reason;
    };
};

export function contradiction_elim_rule(tree,getClickedUniqIds){
    let FirstOccurrenceOfPremiseNode = findFirstOccurrenceOfPremise(tree, getClickedUniqIds);


    FirstOccurrenceOfPremiseNode.conclusion.reason = 'contradiction elim_' + getClickedUniqIds;
    //aktualizacja drzewa
    createTool(tree);

    //zakończenie pracy funkcji
    return true;
}