{
   "formula": {
      "kind": "or",
      "right": {
         "predicate": {
            "name": "C",
            "args": undefined
         }
      },
      "left": {
         "kind": "or",
         "right": {
            "predicate": {
               "name": "Q",
               "args": undefined
            }
         },
         "left": {
            "predicate": {
               "name": "P",
               "args": undefined
            }
         }
      }
   }
}